# README #

This is a simple R package for stochastically generating sequences from finite state grammars.

## Installation

From within R:

```
library(devtools)
install_bitbucket("pmcharrison/ArtificialGrammars")
```

## Sample usage:

```
library(ArtificialGrammars)
generate.sequence(grammar = "rohrmeier", noise = 0.1, min.length = 3, max.length = 10)
```